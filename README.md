**Projeto: Integração Contínua com GitLab e Google Cloud Platform**

---

### Visão Geral:

Este projeto tem como objetivo demonstrar a integração contínua de um pipeline utilizando o GitLab e a Google Cloud Platform (GCP). Utilizaremos o GitLab para versionamento de código, gerenciamento de pipelines e execução de jobs, e a GCP para hospedar e executar nossas aplicações.

### Configuração do Ambiente:

1. **GitLab:**
   - Certifique-se de ter uma conta no GitLab.
   - Crie um novo projeto no GitLab para este projeto.
   - Clone este repositório para sua máquina local:

     ```
     git clone https://gitlab.com/arijunior2020/cicd-gitlab-gcp-runner.git
     ```

2. **Google Cloud Platform (GCP):**
   - Crie uma conta na GCP, se ainda não tiver uma.
   - Configure uma instância de máquina virtual na GCP para hospedar seu aplicativo.
   - Instale o Docker e o GitLab Runner na instância da VM.

### Estrutura do Projeto:

projeto-integracao-gitlab-gcp/

│

├── .gitlab-ci.yml # Arquivo de configuração do pipeline do GitLab

├── Dockerfile # Dockerfile para construir a imagem do aplicativo

├── index.html # Página HTML simples

└── README.md # Este arquivo


### Executando o Pipeline:

1. Configure o arquivo `.gitlab-ci.yml` conforme necessário para seus requisitos específicos.
2. Faça o commit e o push das alterações para o GitLab.
3. Acesse a seção de CI/CD no GitLab para monitorar a execução do pipeline.

### Contato e Suporte:

Se precisar de ajuda ou tiver alguma dúvida, entre em contato conosco através dos seguintes canais:

- Email: [arimateiajunior.tic@gmail.com](mailto:arimateiajunior.tic@gmail.com)
- GitLab: [Arimatéia Júnior](https://gitlab.com/arijunior2020)
- LinkedIn: [Arimatéia Júnior](https://www.linkedin.com/in/arimateiajunior/)

### Contribuições:

Contribuições são bem-vindas! Sinta-se à vontade para abrir uma *issue* ou enviar um *merge request* com suas sugestões e melhorias.

**Divirta-se codificando!** 🚀🔧

--- 
